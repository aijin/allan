
import * as React from 'react';
import { Platform,StyleSheet,ScrollView, Dimensions,Button, View, Text,TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Container, Content, Header, Left, Right, Icon, Item, Input, Card, CardItem,Image} from 'native-base';
import HomeScreen from './screens/Home'
import Book1 from './screens/book1'
import Book2 from './screens/book2'
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Sample_book1" component={Book1} />
        <Stack.Screen name="Sample_book2" component={Book2} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App