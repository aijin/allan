import * as React from 'react';
import { Platform,StyleSheet,ScrollView, Dimensions,Button, View, Text,TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Container, Content, Header, Left, Right, Icon, Item, Input, Card, CardItem,Image} from 'native-base';

var deviceWidth = Dimensions.get('window').width;



function Book2({ navigation }) {
    return (
      <View style={styles.container}>
          <ScrollView horizontal={true} pagingEnabled={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.firstView}>
              <Text style={styles.headerText}>Lorem Ipsum Book 2
  
  "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
  "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."
  
  
  Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris semper dictum elementum. Proin pretium eros vitae felis dictum imperdiet. Phasellus ac nibh tempor, aliquam enim eget, venenatis nunc. Phasellus auctor, dui a tincidunt semper, magna mi tempor mauris, in consectetur sapien erat dapibus risus. Nullam ultrices viverra placerat. Mauris fringilla in arcu sit amet lobortis. Etiam vel interdum velit. Cras ornare volutpat mollis.
  
  Integer aliquet malesuada eros, iaculis convallis arcu vehicula ac. consectetur velit nec vestibulum vestibulum.
  
  Nam non lorem euismod, tempus felis quis, euismod sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sed convallis orci. Nunc eu ultricies leo. In sed felis non arcu hendrerit volutpat vel placerat odio. Nulla scelerisque rutrum ullamcorper. Morbi purus sem, dictum ut varius sed, facilisis vitae est. Ut iaculis odio quam, eget congue mi faucibus nec. Ut velit ante, ultricies sed risus ac, fringilla accumsan eros. Nulla mollis nibh ac commodo vehicula. Fusce a bibendum turpis. Nunc lacus ex, rutrum vitae nisl nec, ornare rhoncus nibh.
  </Text>
            </View>
  
            <View style={styles.secondView}>
              <Text style={styles.headerText}>SeNam non lorem euismod, tempus felis quis, euismod sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur sed convallis orci. Nunc eu ultricies leo. In sed felis non arcu hendrerit volutpat vel placerat odio. Nulla scelerisque rutrum ullamcorper. Morbi purus sem, dictum ut varius sed, facilisis vitae est. Ut iaculis odio quam, eget congue mi faucibus nec. Ut velit ante, ultricies sed risus ac, fringilla accumsan eros. Nulla mollis nibh ac commodo vehicula. Fusce a bibendum turpis. Nunc lacus ex, rutrum vitae nisl nec, ornare rhoncus nibh.
  
  Nulla facilisi. Integer non leo nisi. Praesent accumsan arcu et magna consectetur vehicula. Praesent tempor orci non quam tempus suscipit. Phasellus convallis purus quam. Vestibulum a vestibulum ante. In euismod id massa ut dictum. Morbi quis ornare erat, ac sagittis eros. Vestibulum aliquet, justo vel ultrices lacinia, ipsum massa pharetra massa, et tempus metus dui ut nisi. Maecenas a dapibus tellus. Cras condimentum laoreet nibh, quis efficitur justo tincidunt sit amet.
  
  Aenean consectetur dui sem, vel venenatis nisi vehicula eu. Proin accumsan molestie lectus, sed convallis nisi ornare sed. Ut congue rutrum leo vel vulputate. Curabitur vel orci scelerisque, lacinia libero sit amet, molestie sem. Nam suscipit in enim et hendrerit. Suspendisse potenti. Nulla felis urna, pretium ut mollis eu, hendrerit sed nulla. Ut id sodales tellus, ut semper turpis. Nulla facilisi. Cras eget ultricies leo. Mauris mollis gravida ex, vel aliquam neque sodales et. Ut euismod sed dolor in hendrerit. Ut sapien orci, commodo ac vestibulum eget, feugiat ut nisi. Vestibulum quis enim eget libero aliquet egestas. Fusce luctus viverra elit. Ut sed augue sit amet tortor consequat eleifend eu eget orci.
  
  suscipit, et suscipit felis iaculis.
  
  Nam ut sapien suscipit, rhoncus nisl et, lacinia risus. Nulla quis maximus erat. Donec lacinia, lacus eget gravida aliquet, ligula libero posuere diam, id auctor odio neque nec turpis. Integer nibh tellus, condimentum eget lectus ut, egestas sollicitudin magna. Morbi scelerisque augue lorem, consectetur ullamcorper nulla luctus at. Sed condimentum lorem a sapien faucibus lobortis at iaculis mauris. Quisque id pharetra sapien. In hac habitasse platea dictumst. Donec ipsum sem, condimentum vel magna ac, ultrices eleifend quam. Fusce a ultricies massa. In in dui vel ex laoreet sagittis. Nunc posuere nisl lorem, eget semper tortor placerat in. Praesent auctor non libero id eleifend. Fusce lacinia vitae elit eu convallis. Aliquam ultrices, mi eget </Text>
            </View>
  
            <View style={styles.thirdView}>
              <Text style={styles.headerText}>ThiUt faucibus, lectus sit amet pretium porta, erat libero feugiat neque, vel pharetra tortor libero ut tellus. Sed vitae turpis non libero volutpat malesuada. Vestibulum feugiat ut metus non tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut aliquam lacus at mattis ultricies. Cras fermentum nunc vitae cursus aliquet. Maecenas ligula libero, luctus at eros sed, finibus vestibulum dui. Vivamus tempus quam ac sapien vehicula, nec laoreet risus interdum. Vestibulum dictum, risus at pretium ullamcorper, dolor erat sollicitudin tellus, in aliquam nisl velit in ante.
  
  Nunc quam dolor, aliquet nec purus eget, tincidunt bibendum felis. Fusce sit amet felis ac nunc viverra iaculis. Nunc eget cursus nisi. In elementum sit amet nisl nec consectetur. Integer lacus lacus, luctus eget odio sed, laoreet mollis ante. Nulla sit amet sapien viverra, tristique elit eget, pellentesque risus. Sed interdum nunc sagittis lacus feugiat, eget semper orci tristique. Phasellus volutpat nec tortor eu fringilla. Praesent diam nisi, suscipit at molestie sed, gravida non erat. Morbi sit amet turpis quis dui commodo blandit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis hendrerit diam nec scelerisque lacinia. Praesent vestibulum rhoncus dictum. Aliquam venenatis, mauris quis dictum condimentum, nulla tellus consequat ligula, vitae consequat orci metus et tortor. Donec pellentesque ultrices felis, tempor ullamcorper eros mollis et.
  
  Morbi condimentum lacus et quam facilisis porttitor. Vivamus sapien libero, sodales quis purus at, sodales sagittis dolor. Praesent purus risus, efficitur eu dapibus sed, rutrum vitae nisl. Duis sed augue id odio auctor aliquam. Ut convallis cursus congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam hendrerit vulputate feugiat.
  
  Praesent placerat blandit enim, vel porttitor tellus lobortis in. Donec condimentum sed tortor eget iaculis. Nunc nulla dui, venenatis id est eu, rhoncus blandit massa. Sed vel mattis sapien. Praesent luctus pharetra velit. Integer vitae velit magna. Maecenas at augue suscipit, pulvinar nisl varius, commodo ipsum.
  
  Vivamus eleifend at ipsum ut mollis. Aenean a porttitor mauris, quis porta nulla. Nunc vel neque in dolor dapibus tempor. Proin sit amet tristique velit. Curabitur mauris odio, pellentesque vitae diam ut, tincidunt vulputate libero. Integer iaculis consequat risus, a fringilla elit lacinia a. Morbi vestibulum nunc venenatis elit fringilla, sed blandit sapien venenatis. Sed mollis, nulla ac volutpat aliquam, sem lectus vehicula felis, vitae sodales purus massa a est. Donec et porttitor lectus.
  
  Praesent risus est, sollicitudin in enim nec, ullamcorper rutrum ipsum. Morbi in ipsum nisi. Etiam consequat aliquam elit at pellentesque. Integer egestas erat dolor, ac posuere nulla tincidunt et. Integer iaculis rutrum eros sed auctor. Vivamus sit amet mi ac enim hendrerit dictum. Suspendisse iaculis feugiat dolor, eu fringilla lorem fringilla vel. Donec turpis tellus, tempus sed facilisis vitae, accumsan sed neque. Suspendisse id nunc interdum, pretium tortor ac, ullamcorper neque. Praesent euismod blandit elementum. Aliquam cursus, leo in ornare dictum, eros mi sagittis ligula, sit amet accumsan tortor augue id elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla venenatis diam a lacus efficitur pretium.
  
  Ut sodales fringilla purus ultricies bibendum. Curabitur maximus ut ex eget posuere. Donec tincidunt tempus pretium. Donec sodales faucibus viverra. Donec dignissim purus id sollicitudin laoreet. Ut dapibus hendrerit sem, a venenatis sapien elementum in. Nam faucibus est vel euismod rutrum. Nullam eu est vitae metus egestas egestas. Integer id dictum nisl. Vivamus mattis leo tellus, quis tempor lorem auctor eget. Ut auctor, lacus ut elementum interdum, ipsum mi hendrerit libero, non maximus nisi velit sed mauris. Nullam placerat eget velit ut hendrerit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi ac scelerisque libero. Mauris eu elit volutpat, eleifend orci sed, tincidunt orci. Morbi lobortis id felis nec cursus.
  
  Aenean fermentum augue ut rhoncus elementum. Nunc consectetur dictum gravida. Cras risus lectus, tristique in mauris sed, vehicula maximus leo. Pellentesque mollis ex condimentum est dapibus, auctor convallis quam gravida. Donec accumsan maximus consectetur. Ut rutrum nisl nec tellus congue pellentesque vel sed urna. Donec vulputate eget risus in vestibulum. Sed eleifend pulvinar nisi, et auctor lectus sollicitudin a. Etiam placerat commodo tellus. Etiam dapibus eros at purus egestas dapibus. Pellentesque accumsan dignissim lacus, in placerat purus.
  
  Nam consequat mattis metus, in sodales sapien malesuada id. Mauris id sem sed nisi mollis blandit id nec felis. Fusce eu tempus leo. Duis et lectus tincidunt, congue augue in, molestie turpis. Morbi a eros in purus vestibulum mollis vel et dolor. Etiam felis ex, dignissim sit amet nisi ut, pulvinar feugiat ipsum. Donec cursus metus a feugiat ornare. Fusce placerat scelerisque tellus. Morbi tempor id purus ac tempor. Suspendisse sollicitudin magna eu pulvinar tincidunt.
  
  Nunc accumsan tellus blandit, dictum velit et, varius tellus. Aliquam viverra, dolor id convallis gravida, magna est commodo sem, quis tincidunt magna nisl quis sem. Nam et neque vitae nisl sollicitudin auctor. Mauris turpis neque, condimentum sit amet gravida ut, tincidunt vitae libero. Sed sem mi, ornare varius eleifend vel, auctor sit amet massa. Vivamus vestibulum, magna congue interdum vestibulum, arcu urna feugiat quam, et tempus lectus neque id metus. In finibus sapien id erat euismod cursus. Nam molestie diam libero. Cras eleifend nibh sem, in elementum est rhoncus ut.
  
  Pellentesque bibendum sodales tellus, id molestie purus gravida eget. Integer id nisl urna. Nullam consectetur nisl tempus justo facilisis, ac facilisis justo finibus. Vivamus mattis ex at arcu hendrerit, pellentesque molestie erat malesuada. Sed nibh ante, aliquet et consectetur ut, maximus sollicitudin lacus. Maecenas sagittis pretium rutrum. Nunc ac velit rhoncus, sodales mauris vitae, ornare arcu. Ut fermentum, nisi vel faucibus cursus, turpis mi laoreet neque, eget vehicula odio risus quis dolor. Donec pharetra scelerisque tellus nec aliquet. Donec aliquet ultricies nisi, quis fermentum tellus viverra vitae. </Text>
            </View>
  
            <View style={styles.forthView}>
              <Text style={styles.headerText}>Vestibulum mattis in eros sed consectetur. Vivamus in aliquam lacus, at sollicitudin sem. In tempus lobortis nisl vel maximus. Etiam ante turpis, interdum eget odio sit amet, posuere facilisis est. Fusce posuere tempor tortor, at varius sapien eleifend nec. Phasellus eget porta risus, vitae pellentesque felis. Duis eget interdum dui. Aenean ultricies sapien orci, quis suscipit eros feugiat sit amet. Fusce eu pharetra tortor, vitae fermentum ipsum. Vivamus pharetra a leo placerat condimentum. Ut nisl ipsum, facilisis a tellus quis, auctor fermentum nisl. Suspendisse a magna sed eros pretium pulvinar. Phasellus felis quam, bibendum eget ex eu, auctor suscipit risus. Morbi at lacinia augue. Praesent ac ligula ut diam ullamcorper consequat non nec lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  
  Donec ultrices nisi odio. Sed hendrerit feugiat risus vel semper. Vivamus at dapibus odio. Etiam eget facilisis massa, eu porta eros. Proin mattis eget nisl dapibus luctus. Integer vestibulum libero sagittis efficitur ultrices. Duis quis aliquet sem. In fringilla quam tellus, et tempor sapien hendrerit et. Cras consectetur egestas odio, non aliquam nulla. Fusce vitae mollis tellus. Phasellus nibh nisi, ultricies id volutpat non, suscipit ut velit.
  
  Morbi venenatis diam et augue tincidunt, et ornare justo posuere. Donec nec nunc nec magna aliquam dignissim. Quisque condimentum leo vel libero ornare ornare. Etiam gravida justo sit amet metus ultricies, egestas condimentum augue gravida. Integer pellentesque accumsan orci, bibendum iaculis ante accumsan eu. Donec sed urna orci. Etiam lacinia efficitur quam eget egestas. Sed nec mi id nisl accumsan commodo et eu urna.
  
  Integer varius eu diam vel condimentum. Proin eu nibh risus. Phasellus lobortis urna elit, in pharetra urna scelerisque vitae. Sed viverra elit quis rutrum fermentum. Curabitur sit amet est et purus consectetur porttitor. Phasellus commodo elementum dictum. Integer ultrices non magna eu molestie. Sed nunc sem, eleifend aliquet massa a, ultricies scelerisque dolor. Fusce venenatis pharetra augue, sed consectetur dolor scelerisque vitae.
  
  Suspendisse et molestie enim, ac euismod arcu. Morbi placerat, odio et varius lacinia, diam purus tempor ipsum, ut blandit ligula metus at nunc. Nunc ac lorem libero. Curabitur mollis accumsan mi quis laoreet. Sed laoreet volutpat hendrerit. Cras non bibendum ligula. Quisque mollis interdum nisl, vel convallis ligula tincidunt scelerisque. Quisque placerat erat ut libero tincidunt pharetra. Etiam pharetra leo sed nisi convallis porttitor. Nulla facilisi. Suspendisse bibendum ultrices vulputate. In varius lobortis est, ut elementum urna molestie eu. Duis eget enim quam.
  
  Duis vel iaculis velit. Phasellus vulputate eleifend ante, ac egestas nisl convallis ac. Curabitur nisi orci, pellentesque et dolor sit amet, porttitor mattis enim. Pellentesque dolor lorem, luctus vitae eleifend id, mattis interdum velit. Integer bibendum erat a velit sagittis, eu vestibulum libero congue. Nulla dictum urna magna, hendrerit volutpat ex congue vel. Vivamus tempus a mi ac malesuada. Suspendisse tincidunt at velit eu laoreet. Integer neque lectus, egestas quis tellus id, consequat posuere nisl. Nam interdum mattis nisl et accumsan. Praesent condimentum non ipsum vitae tincidunt. Curabitur fringilla, est vitae dictum ornare, odio lorem dapibus nibh, hendrerit interdum dui orci ac ante. Ut consequat eleifend quam, sit amet pharetra lectus tincidunt non </Text>
            </View>
          </ScrollView>
        </View>
      
    );
  }
   
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#e5e5e5",
    },
    headerText: {
      fontSize: 10,
      textAlign: "center",
      margin: 10,
      color: 'black',
      fontWeight: "bold"
    },
    firstView: {
      width: deviceWidth,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    secondView: {
      width: deviceWidth,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    thirdView: {
      width: deviceWidth,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    forthView: {
      width: deviceWidth,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
  
  });

  export default Book2;