import React, { Component } from 'react';
import { Image , StyleSheet, TouchableOpacity, Alert} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Left, Body, Right } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CardImageExample extends Component {
  addProductToCart = () => {
    Alert.alert('Success', 'The product has been added to your cart')
  }
  render() {
    const { navigation } = this.props;
   
    return (
      
      <Container>
        <Header><Text style={styles.Red}>Recommended Books</Text></Header>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Body>
                  <Text>Sample_book1</Text>
                  <Text note>P199</Text>
                </Body>
              </Left>
            </CardItem>
            <TouchableOpacity
    onPress={() => navigation.navigate('Sample_book1')}  >
            <CardItem cardBody >
              
              <Image   source={{uri: 'https://images-na.ssl-images-amazon.com/images/I/71UypkUjStL.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            </TouchableOpacity>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon name="thumbs-up" size={25} color="blue" />
                  <Text>122 Likes</Text>
                </Button>
              </Left>
              
              <Right>
                <Button transparent onPress={() => this.addProductToCart()}>
              <Icon name="shopping-cart" size={25} />

              <Text>Buy Now</Text>

</Button>
               
              </Right>
            </CardItem>
              <CardItem>
              <Left>
               
                <Body>
                  <Text>Sample_book2</Text>
                  <Text note>P299</Text>
                </Body>
              </Left>
            </CardItem>
            <TouchableOpacity
    onPress={() => navigation.navigate('Sample_book2')}  >
            <CardItem cardBody>
              <Image source={{uri: 'https://images-na.ssl-images-amazon.com/images/I/51hV5vGr4AL._SX326_BO1,204,203,200_.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            </TouchableOpacity>
            <CardItem>

              <Left>
                <Button transparent>
                  <Icon name="thumbs-up" size={25} color="blue" />
                  <Text>100 Likes</Text>
                </Button>
              </Left>
              
              <Right>
                <Button transparent onPress={() => this.addProductToCart()}>
              <Icon name="shopping-cart" size={25} />

              <Text>Buy Now</Text>

</Button>
               
              </Right>
            </CardItem>
          </Card>
        </Content>
        
      </Container>
      
    );
  }
}
const styles = StyleSheet.create({
 
  Red: {
    marginTop: 25,
fontSize: 25,
    color: 'white'
    
  },
  
 
});
 